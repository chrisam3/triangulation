
InstallMethod( SetVertexCoordiantes3DNC,
    "for a list of coordinates and a record",
    [IsDenseList, IsRecord],
    function(coordinates, printRecord)
				printRecord.vertexCoordinates3D := coordinates;
				return printRecord;
    end
);

BindGlobal( "__SIMPLICIAL_TestCoordinatesFormat",
    function(surface, coordinates)
        if Length(coordinates) <> NumberOfVertices(surface) then
          return false;
				fi;
 			if not ForAll(coordinates, IsDenseList) then
          return false;
				fi;
				return AsSet( List(coordinates, c -> Length(c))) = AsSet( [3] );
    end
);

InstallMethod( SetVertexCoordiantes3D,
    "for a polygonal complex without edge ramifications, a list of coordinates and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsDenseList, IsRecord],
    function(surface, coordinates, printRecord)
				if not __SIMPLICIAL_TestCoordinatesFormat(surface, coordinates) then
					Error( " invalid coordinate format " );
				fi;
				return SetVertexCoordiantes3DNC(coordinates, printRecord);
    end
);

InstallOtherMethod( SetVertexCoordiantes3DNC,
    "for a list of coordinates",
    [IsDenseList],
    function(coordinates)
				return SetVertexCoordiantes3DNC(coordinates, rec());
    end
);

InstallOtherMethod( SetVertexCoordiantes3D,
    "for a polygonal complex without edge ramifications and a list of coordinates",
    [IsSimplicialSurface and IsNotEdgeRamified, IsDenseList],
    function(surface, coordinates)
				return SetVertexCoordiantes3D(surface, coordinates, rec());
    end
);

InstallMethod( GetVertexCoordiantes3DNC,
    "for an index and a record",
    [IsCyclotomic, IsRecord],
    function(index, printRecord)
				return 1.0*printRecord.vertexCoordinates3D[index];
    end
);

InstallMethod( GetVertexCoordiantes3D,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
				if not __SIMPLICIAL_TestCoordinatesFormat(surface, printRecord.vertexCoordinates3D) then
					Error( " invalid coordinate format " );
				fi;
				return GetVertexCoordiantes3DNC(index, printRecord);
    end
);

InstallMethod( CalculateParametersOfInnerCircle,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
				local norm, distance, normalize, crossProd, Atan2, res, vertOfFace, P1, P2, P3, d1, d2, d3, incenter, s,
					radius, x, y, z, alpha, beta, gamma, normalVector, lengthNormalVector, normalVector_beta, normalVector_gamma;
				if not __SIMPLICIAL_TestCoordinatesFormat(surface, printRecord.vertexCoordinates3D) then
					Error( " invalid coordinate format " );
				fi;
				norm:=function(v) return Sqrt( v[1]*v[1] + v[2]*v[2] + v[3]*v[3] ); end;
				distance:=function(p,q) return norm(p-q); end;
				normalize:=function(v) return v/norm(v); end;
				crossProd:=function(v,w) return [ v[2]*w[3]-v[3]*w[2], v[3]*w[1]-v[1]*w[3], v[1]*w[2]-v[2]*w[1] ]; end;
				Atan2:=function(y,x)
					if x > 0. then
						return Atan(y/x);
					fi;
					if x < 0. then
						if y > 0. then
							return Atan(y/x)+4*Atan(1.0);
						fi;
						if y = 0. then
							return 4*Atan(1.0);
						fi;
						return Atan(y/x)-4*Atan(1.0);
					fi;
					if y > 0. then
						return 2*Atan(1.0);
					fi;
					if y < 0. then
						return -2*Atan(1.0);
					fi;
					return 0.;
				end;
				res := [];
				for vertOfFace in VerticesOfFaces(surface) do
					if Length(vertOfFace) <> 3 then
						Append(res, [[]]);
						continue;
					fi;
					P1 := GetVertexCoordiantes3DNC(vertOfFace[1], printRecord);
					P2 := GetVertexCoordiantes3DNC(vertOfFace[2], printRecord);
					P3 := GetVertexCoordiantes3DNC(vertOfFace[3], printRecord);
					# calculate distances
					d1 := distance(P2,P3);
					d2 := distance(P1,P3);
					d3 := distance(P1,P2);
					# calculate coordiantes of incenter
					incenter := ( d1*P1 + d2*P2 + d3*P3 ) / ( d1 + d2 + d3 );
					# calculate radius
					s := ( d1 + d2 + d3 ) / 2;
					radius := Sqrt( s * ( s - d1 ) * ( s - d2 ) * ( s - d3 ) ) / s;
					# calculate rotation angles (from x-y-plane)
					z := normalize(crossProd( P2-P1, P3-P1 ));
					x := normalize((P1+P2)/2 - P3);
					y := crossProd(z, x);
					alpha := Atan2(-z[2], z[3]);
					beta := Asin(z[1]);
					gamma := Atan2(-y[1], x[1]);
					# calculat rotation angles and length of normal vector
					normalVector := crossProd( P1-P2, P1-P3 );
					lengthNormalVector := 4*radius;
					normalVector_beta := Atan2(-1.0*normalVector[3], 1.0*normalVector[1]);
					normalVector_gamma := -Acos(1.0*normalVector[2]/norm(normalVector));
					Append(res, [ [incenter, radius, [ alpha, beta, gamma ], [ 0., normalVector_beta, normalVector_gamma], lengthNormalVector ] ]);
				od;
				printRecord.innerCircles := res;
				return printRecord;
    end
);

InstallMethod( ActivateInnerCircles,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			local i;
			if not IsBound(printRecord.innerCircles) then
				printRecord := CalculateParametersOfInnerCircle(surface, printRecord);
			fi;
			for i in [1..NumberOfFaces(surface)] do
				printRecord := ActivateInnerCircle(surface, i, printRecord);
			od;
			return printRecord;
    end
);

InstallMethod( DeactivateInnerCircles,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			printRecord.drawInnerCircles := [];
			return printRecord;
    end
);

InstallMethod( ActivateInnerCircle,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.innerCircles) then
				printRecord := CalculateParametersOfInnerCircle(surface, printRecord);
			fi;
			if not IsBound(printRecord.drawInnerCircles) then
				printRecord.drawInnerCircles := [];
			fi;
			printRecord.drawInnerCircles[index] := true;
			return printRecord;
    end
);

InstallMethod( DeactivateInnerCircle,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.drawInnerCircles) then
				return printRecord;
			fi;
			printRecord.drawInnerCircles[index] := false;
			return printRecord;
    end
);

InstallMethod( IsInnerCircleActive,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.innerCircles) then
					return false;
				fi;
				if not IsBound(printRecord.drawInnerCircles) or (index <= 0) then
					return false;
				fi;
				if not IsBound(printRecord.drawInnerCircles[index]) then
					return false;
				fi;
				return printRecord.drawInnerCircles[index] = true;
    end
);

InstallMethod( ActivateNormalOfInnerCircles,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			local i;
			if not IsBound(printRecord.innerCircles) then
				printRecord := CalculateParametersOfInnerCircle(surface, printRecord);
			fi;
			for i in [1..NumberOfFaces(surface)] do
				printRecord := ActivateNormalOfInnerCircle(surface, i, printRecord);
			od;
			return printRecord;
    end
);

InstallMethod( DeactivateNormalOfInnerCircles,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			printRecord.drawNormalOfInnerCircles := [];
			return printRecord;
    end
);

InstallMethod( ActivateNormalOfInnerCircle,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.innerCircles) then
				printRecord := CalculateParametersOfInnerCircle(surface, printRecord);
			fi;
			if not IsBound(printRecord.drawNormalOfInnerCircles) then
				printRecord.drawNormalOfInnerCircles := [];
			fi;
			printRecord.drawNormalOfInnerCircles[index] := true;
			return printRecord;
    end
);

InstallMethod( DeactivateNormalOfInnerCircle,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.drawNormalOfInnerCircles) then
				return printRecord;
			fi;
			printRecord.drawNormalOfInnerCircles[index] := false;
			return printRecord;
    end
);

InstallMethod( IsNormalOfInnerCircleActive,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.innerCircles) then
					return false;
				fi;
				if not IsBound(printRecord.drawNormalOfInnerCircles) or (index <= 0) then
					return false;
				fi;
				if not IsBound(printRecord.drawNormalOfInnerCircles[index]) then
					return false;
				fi;
				return printRecord.drawNormalOfInnerCircles[index] = true;
    end
);

InstallMethod( CalculateParametersOfEdges,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
				local norm, distance, Atan2, res, vertOfEdge, P1, P2, d, mid, beta, gamma;
				if not __SIMPLICIAL_TestCoordinatesFormat(surface, printRecord.vertexCoordinates3D) then
					Error( " invalid coordinate format " );
				fi;
				norm:=function(v) return Sqrt( v[1]*v[1] + v[2]*v[2] + v[3]*v[3] ); end;
				distance:=function(p,q) return norm(p-q); end;
				Atan2:=function(y,x)
					if x > 0. then
						return Atan(y/x);
					fi;
					if x < 0. then
						if y > 0. then
							return Atan(y/x)+4*Atan(1.0);
						fi;
						if y = 0. then
							return 4*Atan(1.0);
						fi;
						return Atan(y/x)-4*Atan(1.0);
					fi;
					if y > 0. then
						return 2*Atan(1.0);
					fi;
					if y < 0. then
						return -2*Atan(1.0);
					fi;
					return 0.;
				end;
				res := [];
				for vertOfEdge in VerticesOfEdges(surface) do
					P1 := GetVertexCoordiantes3DNC(vertOfEdge[1], printRecord);
					P2 := GetVertexCoordiantes3DNC(vertOfEdge[2], printRecord);
					# calculate distance
					d := distance(P1,P2);
					# calculate coordiantes of mid of edge
					mid := ( P1 + P2 ) / 2;
					# calculate rotation angles (from y-direction)
					beta := Atan2(-1.0*(P2[3]-P1[3]), 1.0*(P2[1]-P1[1]));
					gamma := -Acos(1.0*(P2[2]-P1[2])/d);
					Append(res, [ [mid, d, [ 0., beta, gamma ] ] ]);
				od;
				printRecord.edges := res;
				return printRecord;
    end
);

InstallMethod( ActivateEdges,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			if not IsBound(printRecord.edges) then
				printRecord := CalculateParametersOfEdges(surface, printRecord);
			fi;
			printRecord.drawEdges := [];
			return printRecord;
    end
);

InstallMethod( DeactivateEdges,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			local i;
			for i in [1..NumberOfEdges(surface)] do
				printRecord := DeactivateEdge(surface, i, printRecord);
			od;
			return printRecord;
    end
);

InstallMethod( ActivateEdge,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.edges) then
				printRecord := CalculateParametersOfEdges(surface, printRecord);
			fi;
			if not IsBound(printRecord.drawEdges) then
				printRecord.drawEdges := [];
				return printRecord;
			fi;
			printRecord.drawEdges[index] := true;
			return printRecord;
    end
);

InstallMethod( DeactivateEdge,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.drawEdges) then
				printRecord.drawEdges := [];
			fi;
			printRecord.drawEdges[index] := false;
			return printRecord;
    end
);

InstallMethod( IsEdgeActive,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.edges) then
					return false;
				fi;
				if not IsBound(printRecord.drawEdges) then
					return true;
				fi;
				if (index <= 0) then
					return false;
				fi;
				if not IsBound(printRecord.drawEdges[index]) then
					return true;
				fi;
				return printRecord.drawEdges[index] = true;
    end
);

InstallMethod( ActivateFaces,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			printRecord.drawFaces := [];
			return printRecord;
    end
);

InstallMethod( DeactivateFaces,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			local i;
			for i in [1..NumberOfFaces(surface)] do
				printRecord := DeactivateFace(surface, i, printRecord);
			od;
			return printRecord;
    end
);

InstallMethod( ActivateFace,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.drawFaces) then
				printRecord.drawFaces := [];
				return printRecord;
			fi;
			printRecord.drawFaces[index] := true;
			return printRecord;
    end
);

InstallMethod( DeactivateFace,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.drawFaces) then
				printRecord.drawFaces := [];
			fi;
			printRecord.drawFaces[index] := false;
			return printRecord;
    end
);

InstallMethod( IsFaceActive,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
				if not IsBound(printRecord.drawFaces) then
					return true;
				fi;
				if (index <= 0) then
					return false;
				fi;
				if not IsBound(printRecord.drawFaces[index]) then
					return true;
				fi;
				return printRecord.drawFaces[index] = true;
    end
);

InstallMethod( SetTransparencyJava,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsFloat, IsRecord],
    function(surface, index, value, printRecord)
            if not IsBound(printRecord.FaceTransparency) then
                printRecord.FaceTransparency := [];
            fi;
            printRecord.FaceTransparency[index] := value;
            return printRecord;
    end
);

InstallMethod( RemoveTransparencyJava,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
            if IsBound(printRecord.FaceTransparency[index]) then
                Unbind(printRecord.FaceTransparency[index]);
                return printRecord;
            fi;
            return printRecord;
    end
);

InstallMethod( GetTransparencyJava,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
                if not IsBound(printRecord.FaceTransparency) then
                    return 1;
                fi;
                if (index <= 0) then
                    return 0;
                fi;
                if not IsBound(printRecord.FaceTransparency[index]) then
                    return 1;
                fi;
                return printRecord.FaceTransparency[index];
    end
);

InstallMethod( ActivateVertices,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			if not IsBound(printRecord.vertexCoordinates3D) then
				Error(" The 3D-coordinates of the vertices are not set ");
			fi;
			printRecord.drawVertices := [];
			return printRecord;
    end
);

InstallMethod( DeactivateVertices,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
			local i;
			for i in [1..NumberOfVertices(surface)] do
				printRecord := DeactivateVertex(surface, i, printRecord);
			od;
			return printRecord;
    end
);

InstallMethod( ActivateVertex,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.vertexCoordinates3D) then
				Error(" The 3D-coordinates of the vertices are not set ");
			fi;
			if not IsBound(printRecord.drawVertices) then
				printRecord.drawVertices := [];
				return printRecord;
			fi;
			printRecord.drawVertices[index] := true;
			return printRecord;
    end
);

InstallMethod( DeactivateVertex,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.drawVertices) then
				printRecord.drawVertices := [];
			fi;
			printRecord.drawVertices[index] := false;
			return printRecord;
    end
);

InstallMethod( IsVertexActive,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
			if not IsBound(printRecord.vertexCoordinates3D) then
					return false;
				fi;
				if not IsBound(printRecord.drawVertices) then
					return true;
				fi;
				if (index <= 0) then
					return false;
				fi;
				if not IsBound(printRecord.drawVertices[index]) then
					return true;
				fi;
				return printRecord.drawVertices[index] = true;
    end
);

InstallMethod( SetFaceColour,
    "for a polygonal complex without edge ramifications, an index, a string and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsString, IsRecord],
    function(surface, index, colour, printRecord)
				if not IsBound(printRecord.faceColours) then
					printRecord.faceColours := [];
				fi;
				printRecord.faceColours[index] := colour;
				return printRecord;
    end
);

InstallMethod( GetFaceColour,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
				local default;
				default := "0xFFFF00";
				if not IsBound(printRecord.faceColours) or (index <= 0) then
					return default;
				fi;
				if not IsBound(printRecord.faceColours[index]) then
					return default;
				fi;
				return printRecord.faceColours[index];
    end
);

InstallMethod( SetFaceColours,
    "for a polygonal complex without edge ramifications, a list and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsDenseList, IsRecord],
    function(surface, faceColours, printRecord)
				printRecord.faceColours := faceColours;
				return printRecord;
    end
);

InstallMethod( GetFaceColours,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
				if not IsBound(printRecord.faceColours) then
					return [];
				fi;
				return printRecord.faceColours;
    end
);

InstallMethod( SetVertexColour,
    "for a polygonal complex without edge ramifications, an index, a string and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsString, IsRecord],
    function(surface, index, colour, printRecord)
				if not IsBound(printRecord.vertexColours) then
					printRecord.vertexColours := [];
				fi;
				printRecord.vertexColours[index] := colour;
				return printRecord;
    end
);

InstallMethod( GetVertexColour,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
				local default;
				default := "0xF58137";
				if not IsBound(printRecord.vertexColours) or (index <= 0) then
					return default;
				fi;
				if not IsBound(printRecord.vertexColours[index]) then
					return default;
				fi;
				return printRecord.vertexColours[index];
    end
);

InstallMethod( SetVertexColours,
    "for a polygonal complex without edge ramifications, a list and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsDenseList, IsRecord],
    function(surface, vertexColours, printRecord)
				printRecord.vertexColours := vertexColours;
				return printRecord;
    end
);

InstallMethod( GetVertexColours,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
				if not IsBound(printRecord.vertexColours) then
					return [];
				fi;
				return printRecord.vertexColours;
    end
);

InstallMethod( SetEdgeColour,
    "for a polygonal complex without edge ramifications, an index, a string and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsString, IsRecord],
    function(surface, index, colour, printRecord)
				if not IsBound(printRecord.edgeColours) then
					printRecord.edgeColours := [];
				fi;
				printRecord.edgeColours[index] := colour;
				return printRecord;
    end
);

InstallMethod( GetEdgeColour,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
				local default;
				default := "0xff0000";
				if not IsBound(printRecord.edgeColours) or (index <= 0) then
					return default;
				fi;
				if not IsBound(printRecord.edgeColours[index]) then
					return default;
				fi;
				return printRecord.edgeColours[index];
    end
);

InstallMethod( SetEdgeColours,
    "for a polygonal complex without edge ramifications, a list and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsDenseList, IsRecord],
    function(surface, edgeColours, printRecord)
				printRecord.edgeColours := edgeColours;
				return printRecord;
    end
);

InstallMethod( GetEdgeColours,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
				if not IsBound(printRecord.EdgeColours) then
					return [];
				fi;
				return printRecord.EdgeColours;
    end
);

InstallMethod( SetCircleColour,
    "for a polygonal complex without edge ramifications, an index, a string and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsString, IsRecord],
    function(surface, index, colour, printRecord)
				if not IsBound(printRecord.circleColours) then
					printRecord.circleColours := [];
				fi;
				printRecord.circleColours[index] := colour;
				return printRecord;
    end
);

InstallMethod( GetCircleColour,
    "for a polygonal complex without edge ramifications, an index and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsCyclotomic, IsRecord],
    function(surface, index, printRecord)
				local default;
				default := "0x000000";
				if not IsBound(printRecord.circleColours) or (index <= 0) then
					return default;
				fi;
				if not IsBound(printRecord.circleColours[index]) then
					return default;
				fi;
				return printRecord.circleColours[index];
    end
);

InstallMethod( SetCircleColours,
    "for a polygonal complex without edge ramifications, a list and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsDenseList, IsRecord],
    function(surface, circleColours, printRecord)
				printRecord.circleColours := circleColours;
				return printRecord;
    end
);

InstallMethod( GetCircleColours,
    "for a polygonal complex without edge ramifications and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsRecord],
    function(surface, printRecord)
				if not IsBound(printRecord.circleColours) then
					return [];
				fi;
				return printRecord.circleColours;
    end
);


# general method
InstallMethod( DrawSurfaceToJavaScriptCalculate,
    "for a polygonal complex without edge ramifications, a filename and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsString, IsRecord, IsBool],
    function(surface, fileName, printRecord, calculate)
                local file, output, template, coords, i, j, colour, vertOfFace, vertOfEdge, parametersOfCircle, parametersOfEdge, temp;

        # Make the file end with .html
        if not EndsWith( fileName, ".html" ) then
            fileName := Concatenation( fileName, ".html" );
        fi;

        # Try to open the given file
        file := Filename( DirectoryCurrent(), fileName ); #TODO allow absolute paths
        output := OutputTextFile( file, false ); # override other files
        if output = fail then
            Error(Concatenation("File ", String(file), " can't be opened.") );
        fi;
        SetPrintFormattingStatus( output, false );

                template := __SIMPLICIAL_ReadTemplateFromFile("/pkg/simplicial-surfaces/doc/JS_Header.html.template");
                AppendTo( output, template );

        # Set coordinates of vertices
        if not __SIMPLICIAL_TestCoordinatesFormat(surface, printRecord.vertexCoordinates3D) then
                    Error( " invalid coordinate format " );
                fi;
                for i in [1..NumberOfVertices(surface)] do
                    coords := GetVertexCoordiantes3DNC(i, printRecord);
                    AppendTo(output, "\t\tallpoints.push(new PMPoint(", coords[1], ",", coords[2], ",", coords[3], "));\n");
                od;

        # Add points to scenario
                for i in [0..(NumberOfVertices(surface)-1)] do
                    if IsVertexActive(surface, i+1, printRecord) then
                        colour := GetVertexColour(surface, i+1, printRecord);
                        if not StartsWith(colour, "0x") then
                            colour := Concatenation("\"", colour, "\"");
                        fi;
                        AppendTo(output, "\t\tvar points_material", i, " = new THREE.MeshBasicMaterial( {color: ", colour, " } );\n");
                        AppendTo(output, "\t\tpoints_material", i, ".side = THREE.DoubleSide;\n");
                        AppendTo(output, "\t\tpoints_material", i, ".transparent = true;\n");
                        AppendTo(output, "\t\t// draw a node as a sphere of radius 0.05\n");
                        AppendTo(output, "\t\tallpoints[", i, "].makesphere(0.05,points_material", i, ");\n");
                        AppendTo(output, "\t\tallpoints[", i, "].makelabel(", i+1, ");\n");
                    fi;
                od;
                template := __SIMPLICIAL_ReadTemplateFromFile("/pkg/simplicial-surfaces/doc/JS_associate_points_init_faces.html.template");
                AppendTo( output, template );

                # Add Faces to scenario
                for i in [1..(NumberOfFaces(surface))] do
                    if IsFaceActive(surface, i, printRecord) then
                        vertOfFace := VerticesOfFaces(surface)[i];
                        colour := GetFaceColour(surface, i, printRecord);
                        if not StartsWith(colour, "0x") then
                            colour := Concatenation("\"", colour, "\"");
                        fi;
                        AppendTo(output, "\t\tvar face", i, " = new THREE.Geometry();\n");
                        for j in [1..3] do
                            AppendTo(output, "\t\tface", i, ".vertices.push(allpoints[", vertOfFace[j]-1, "\].vector);\n");
                        od;
                        AppendTo(output, "\t\tcentroids.push(computeCentroid(face", i, "));\n");
                        AppendTo(output, "\t\tvar face", i, "_material = new THREE.MeshBasicMaterial ({color: ", colour, ", transparent: true, opacity: ", GetTransparencyJava(surface, i, printRecord) , ",side: THREE.DoubleSide , depthWrite: true,depthTest: true, } );\n");
                        AppendTo(output, "\t\tface", i, ".faces.push(new THREE.Face3(0, 1, 2 ,undefined, undefined, 0));\n");
                        AppendTo(output, "\t\tvar face", i, "_obj = new THREE.Face3(0,1,2,undefined, undefined, 0);\n");
                        AppendTo(output, "\t\tobj.add( new THREE.Mesh(face", i, ", face", i, "_material) );\n");
                    fi;
                od;

                AppendTo(output, "\n\n");
                for i in [1..(NumberOfFaces(surface))] do
                    if IsInnerCircleActive(surface, i, printRecord) then
                        parametersOfCircle := printRecord.innerCircles[i];
                        colour := GetCircleColour(surface, i, printRecord);
                        if not StartsWith(colour, "0x") then
                            colour := Concatenation("\"", colour, "\"");
                        fi;
                        AppendTo(output, "\t\tvar circle = Circle(", parametersOfCircle[2], ", ", parametersOfCircle[1][1], ", ",
                            parametersOfCircle[1][2], ", ", parametersOfCircle[1][3], ", ", parametersOfCircle[3][1], ", ",
                            parametersOfCircle[3][2], ", ", parametersOfCircle[3][3], ", ", colour, ");\n");
                        AppendTo(output, "\t\tobj.add(circle);\n");
                    fi;
                od;
                AppendTo(output, "\n\n");

                for i in [1..(NumberOfFaces(surface))] do
                    if IsNormalOfInnerCircleActive(surface, i, printRecord) then
                        parametersOfCircle := printRecord.innerCircles[i];
                        colour := GetCircleColour(surface, i, printRecord);
                        if not StartsWith(colour, "0x") then
                            colour := Concatenation("\"", colour, "\"");
                        fi;
                        AppendTo(output, "\t\tvar normalVector = Edge(", parametersOfCircle[5], ", 0.002, ", parametersOfCircle[1][1], ", ",
                            parametersOfCircle[1][2], ", ", parametersOfCircle[1][3], ", ", parametersOfCircle[4][1], ", ",
                            parametersOfCircle[4][2], ", ", parametersOfCircle[4][3], ", ", colour, ");\n");
                        AppendTo(output, "\t\tobj.add(normalVector);\n");
                    fi;
                od;
                AppendTo(output, "\n\n");

                # Add Edges to scenario
                template := __SIMPLICIAL_ReadTemplateFromFile("/pkg/simplicial-surfaces/doc/JS_init_edges.html.template");
                AppendTo( output, template );
                if not IsBound(printRecord.edges) then
                    printRecord := CalculateParametersOfEdges(surface, printRecord);
                fi;
                if calculate then
                    printRecord := CalculateParametersOfEdges(surface, printRecord);
                fi;
                for i in [1..(NumberOfEdges(surface))] do
                    if IsEdgeActive(surface, i, printRecord) then
                        parametersOfEdge := printRecord.edges[i];
                        colour := GetEdgeColour(surface, i, printRecord);
                        if not StartsWith(colour, "0x") then
                            colour := Concatenation("\"", colour, "\"");
                        fi;
                        AppendTo(output, "\t\tvar edge = Edge(", parametersOfEdge[2], ", 0.01, ", parametersOfEdge[1][1], ", ",
                            parametersOfEdge[1][2], ", ", parametersOfEdge[1][3], ", ", parametersOfEdge[3][1], ", ",
                            parametersOfEdge[3][2], ", ", parametersOfEdge[3][3], ", ", colour, ");\n");
                        AppendTo(output, "\t\tobj.add(edge);\n");
                    fi;
                od;

                if IsBound(printRecord.FaceTransparency) then
                    template := __SIMPLICIAL_ReadTemplateFromFile("/pkg/simplicial-surfaces/doc/JS_FooterFirst.html.template");
                    AppendTo( output, template );
                    for i in [1..(NumberOfFaces(surface))] do
                        if IsBound(printRecord.FaceTransparency[i]) then
                            #face1_material.opacity = 0;
                            AppendTo(output, "\t\t\tface", i , "_material.opacity = ", printRecord.FaceTransparency[i], ";\n");
                        fi;
                    od;
                    template := __SIMPLICIAL_ReadTemplateFromFile("/pkg/simplicial-surfaces/doc/JS_FooterSecond.html.template");
                    AppendTo( output, template );
                else
                    template := __SIMPLICIAL_ReadTemplateFromFile("/pkg/simplicial-surfaces/doc/JS_Footer.html.template");
                    AppendTo( output, template );
                fi;

                CloseStream(output);
        return printRecord;
    end
);



InstallMethod( DrawSurfaceToJavaScript,
    "for a polygonal complex without edge ramifications, a filename and a record",
    [IsSimplicialSurface and IsNotEdgeRamified, IsString, IsRecord],
    function(surface, fileName, printRecord)
        return DrawSurfaceToJavaScriptCalculate(surface,fileName,printRecord,true);
    end
);

InstallMethod( Signum,
    [IsFloat],
    function(x)
        if x > 0. then
            return 1.;
        fi;
        if x = 0. then
            return 0.;
        else 
            return -1.;
        fi;
    end
);

InstallMethod( FlEq,
    [IsFloat,IsFloat,IsFloat],
    function(x,y,epsilon)
        # check if x is equal to y with a numerical error margin, equivalent to being both FlLeq and FlGeq
        if x > y-epsilon then
            if x < y + epsilon then
                return true;
            fi;
        fi;
        return false;
    end
);

InstallMethod( FlVEq,
    [IsList,IsList,IsFloat],
    function(x,y,epsilon)
        # check if List x is equal to List y with a numerical error margin, equivalent to being both FlLeq and FlGeq
        if x[1] > y[1]-epsilon and x[1] < y[1] + epsilon then
            if x[2] > y[2]-epsilon and x[2] < y[2] + epsilon then
                if x[3] > y[3]-epsilon and x[3] < y[3] + epsilon then
                    return true;
                fi;
            fi;
        fi;
        return false;
    end
);

InstallMethod( FlLeq,
    [IsFloat,IsFloat,IsFloat],
    function(x,y,epsilon)
        # check if x is less or equal to y with a numerical error margin
        if x < y+epsilon then
            return true;
        fi;
        return false;
    end
);
InstallMethod( FlGeq,
    [IsFloat,IsFloat,IsFloat],
    function(x,y,epsilon)
        # check if x is greater or equal to y with a numerical error margin
        if y < x+epsilon then
            return true;
        fi;
        return false;
    end
);

InstallMethod( Crossproduct,
    [IsList, IsList],
    function(x,y)
        return [x[2]*y[3]-x[3]*y[2],x[3]*y[1]-x[1]*y[3],x[1]*y[2]-x[2]*y[1]];
    end
);

InstallMethod( NumericalUniqueListOfLists,
    [IsList, IsFloat],
    function(list, eps)
        local n, I, i, unique;
        
        n := Length(list);
        I := [2..n];
        unique := [];
        unique[1] := list[1];
        
        for i in I do
            if ForAll(unique, x-> not FlVEq(x,list[i],eps)) then
                unique[Length(unique)+1] := list[i];
            fi;
        od;
        return unique;
    end
);

InstallMethod( ProjectLineOnTriangle,
    [IsList, IsList, IsFloat, IsList],
    function(x,l,d,normal)
        local lambda, p;
    
        lambda := (d - x*normal) / (l*normal);
        p := x+lambda*l;
        return [p,lambda];
    end
);

InstallMethod( PointOnPlaneInTriangle,
    [IsList, IsList, IsFloat],
    function(vertices, p, epsilon)
        local v, val_1, val_2, one;
        
        v := SolutionMat(vertices,p);
        if v <> fail then
            val_1 := Minimum(v[1],v[2],v[3]);
            val_2 := Maximum(v[1],v[2],v[3]);
            one := v[1] + v[2] + v[3];
            
            return [FlLeq(0.,val_1,epsilon) and FlLeq(val_2,1.,epsilon),false];
        else
            return [true,true];
        fi;
    end
);

InstallMethod( PointOnPlaneInTriangleAlt,
    [IsList, IsList, IsFloat],
    function(vertices, p, epsilon)
        local B, v, val_1, val_2, one;
    
        B := [vertices[1]-vertices[3],vertices[2]-vertices[3]];
        p := p-vertices[3];
        v := SolutionMat(B,p);
        if v <> fail then
            v[3] := 1 - v[1] - v[2];
            val_1 := Minimum(v[1],v[2],v[3]);
            val_2 := Maximum(v[1],v[2],v[3]);
            one := v[1] + v[2] + v[3];
            
            return [FlLeq(0.,val_1,epsilon) and FlLeq(val_2,1.,epsilon) and FlEq(one, 1., epsilon),false];
        else    
            return [false,true];
        fi;
    end
);


InstallMethod( PushLineToEdges,
    [IsList, IsList, IsList, IsList, IsList, IsFloat],
    function(a1, a2, a3, x, l, epsilon)
        local c_to_vertex, normal, o1, o2, o3, alpha_1, alpha_2, alpha_3, alpha, abs_alpha, val_1, val_2, po1, al1, al2, po2, p1, p2;
        
        c_to_vertex := false;
        
        normal := Crossproduct(a2-a1,a3-a1);
        
        o1 := Crossproduct(a1-a2,normal);
        o2 := Crossproduct(a2-a3,normal);
        o3 := Crossproduct(a3-a1,normal);
        if (o1*l) = 0. then
            alpha_1 := 10000000000000.;
        else
            alpha_1 := -( (o1*x) - (a1*o1) )/ (o1*l);
        fi;
        
        if (o2*l) = 0. then
            alpha_2 := 10000000000000.;
        else
            alpha_2 := - ( (o2*x) - (a2*o2) ) / (o2*l);
        fi;
        
        if (o3*l) = 0. then
            alpha_3 := 10000000000000.;
        else
            alpha_3 := - ( (o3*x) - (a3*o3) ) / (o3*l);
        fi;
        
        alpha := [alpha_1, alpha_2, alpha_3];
        abs_alpha := [AbsoluteValue(alpha_1),AbsoluteValue(alpha_2),AbsoluteValue(alpha_3)];
        
        
        
        
        po1 := PositionMinimum(abs_alpha);
        al1 := alpha[po1];
        
        Remove(alpha, po1);
        Remove(abs_alpha, po1);
        
        
        po2 := PositionMinimum(abs_alpha);
        al2 := alpha[po2];
        
        Remove(alpha,po2);
        Remove(abs_alpha, po1);
        
        if FlEq(al1,al2,epsilon) then
            # then the line pushes to a vertex
    
            al2 := alpha[1];
            c_to_vertex := true;
            
        fi;    
        if FlEq(al2,alpha[1],epsilon) then
            # then the line also pushes to a vertex
            # a1 = a2 = alpha[1] cant be true because of geometry (the three lines of triangle cant intersect in one point)
            c_to_vertex := true;
    
        fi;
        
        p1 := x + al1*l;
        p2 := x + al2*l;
        
        
        
        return [p1,p2,c_to_vertex, po1, po2];
    end
);

InstallMethod( Retriangulate,
    [IsTriangularComplex and IsNotEdgeRamified, IsRecord, IsList, IsList, IsList, IsList],
    function(surf, reco, params, inters, res, li)
        local triang, surf1, surf2, surf3, surf4, v1, v2, v3, v4, v5, v6, verticesPos1, verticesPos2, verticesPos3, verticesPos4, vertex, J1, J2, s1, s2, s3, s4, temp, j, eps, p1, p2;
        # need to split up the triangle in 4 parts here
        
        j:= li[1];
        eps := li[2];
        
        vertex := 0;
        
        triang := params[1][j];
        
        surf1 := SimplicialSurfaceByVerticesInFaces( [[1,2,3]] );
        surf2 := SimplicialSurfaceByVerticesInFaces( [[1,2,3]] );
        surf3 := SimplicialSurfaceByVerticesInFaces( [[1,2,3]] );
        surf4 := SimplicialSurfaceByVerticesInFaces( [[1,2,3]] );
        
        p1 := 1;
        p2 := 2;
        
        v1 := 0.;
        # test which edge res[1] and res[2] lie on
        v2 := 0.;
        v3 := 0.;
        v4 := 0.;
        v5 := 0.;
        v6 := 0.;
        
        # need to know which vertex can be connected with the two points from the at the boundary to form a triangle
        # this is the one that is incident to both of the selected edges in PushLineToEdges
        # the method to find out which of the edges it are removes an element in between calculations which is why these if clauses are kind of weird looking
        
        if res[4] = 1 and res[5] = 2 then
            vertex := 1;
        fi;
        
        if res[4] = 1 and res[5] = 1 then
            vertex := 2;
        fi;
        
        if res[4] = 2 and res[5] = 1 then
            vertex := 2;
            p1 := 2;
            p2 := 1;
        fi;
        
        if res[4] = 2 and res[5] = 2 then
            vertex := 3;
            p1 := 2;
            p2 := 1;
        fi;
        
        if res[4] = 3 and res[5] = 1 then
            vertex := 1;
            p1 := 2;
            p2 := 1;
        fi;
        
        if res[4] = 3 and res[5] = 2 then
            vertex := 3;
        fi;
        
        v2 := res[p1];
        v3 := res[p2];
        
        v1 := triang[vertex];
        
        temp := [1,2,3];
        
        Remove(temp,Position(temp,vertex));
        
        v4 := triang[temp[1]];
        v6 := triang[temp[2]];
        
        v5 := (v4+v6)/(2.);
        
        verticesPos1 :=[v1,
                        v2,
                        v3];
        verticesPos2 :=[v2,
                        v4,
                        v5];
                        
        verticesPos3 :=[v2,
                        v5,
                        v3];
        
        verticesPos4 :=[v3,
                        v5,
                        v6];
                 
        J1 := ShallowCopy(Faces(surf));
        
        
        s1 := DisjointUnion(surf, surf1);
        surf := s1[1];
        
        s2 := DisjointUnion(surf, surf2);
        surf := s2[1];
        
        s3 := DisjointUnion(surf, surf1);
        surf := s3[1];
        
        s4 := DisjointUnion(surf, surf2);
        surf := s4[1];
        
        J2 := ShallowCopy(Faces(surf));
        
        SubtractSet(J2,J1);
        
        surf := RemoveFaces(surf,[j]);
        
        params[1][j] := [];
        params[1][J2[1]] := [v1, v2, v3, Crossproduct(v2-v1,v3-v1), VerticesOfFace(surf,J2[1])];
        params[1][J2[2]] := [v2, v4, v5, Crossproduct(v4-v2,v5-v2), VerticesOfFace(surf,J2[2])];
        params[1][J2[3]] := [v2, v5, v3, Crossproduct(v5-v2,v3-v2), VerticesOfFace(surf,J2[3])];
        params[1][J2[4]] := [v3, v5, v6, Crossproduct(v5-v3,v6-v3), VerticesOfFace(surf,J2[4])];
        
         # remove faces that have been replaced
        Remove(params[2],Position(params[2],j));
        Add(params[2], J2[1],J2[1]);
        Add(params[2], J2[2], J2[2]);
        Add(params[2], J2[3],J2[3]);
        Add(params[2], J2[4], J2[4]);
        
        
        return [surf,params];
        
    end
);

InstallMethod( RetriangulateEasy,
    [IsTriangularComplex and IsNotEdgeRamified, IsRecord, IsList, IsList, IsList, IsList],
    function(surf, reco, params, inters, res, li)
        local current, surf1, surf2, v1, v2, v3, v4, verticesPos1, verticesPos2, J1, J2, s1, s2, j, eps;
        # only need to split up the triangle in 2 parts here
        
       
        j:= li[1];
        eps := li[2];
        current := params[1][j];
        surf1 := SimplicialSurfaceByVerticesInFaces( [[1,2,3]] );
        surf2 := SimplicialSurfaceByVerticesInFaces( [[1,2,3]] );
        v1 := 0.;
        v2 := 0.;
        v3 := res[1];
        v4 := res[2];
        # test which vertex of the face is covered by the intersection line
        if FlVEq(v3,current[1], eps) or FlVEq(v4,current[1], eps) then
            v1 := current[2];
            v2 := current[3];
        fi;
        
        if FlVEq(v3,current[2], eps) or FlVEq(v4,current[2], eps) then
            v1 := current[1];
            v2 := current[3];
        fi;
        
        if FlVEq(v3,current[3], eps) or FlVEq(v4,current[3], eps) then
            v1 := current[1];
            v2 := current[2];
        fi;
        
        verticesPos1 :=[v1,
                        res[1],
                        res[2] ];
        verticesPos2 :=[v2,
                        res[1],
                        res[2] ];
        # TODO: ACTIVATE DEACTIVATE FACES
        J1 := ShallowCopy(Faces(surf));
        
        
        s1 := DisjointUnion(surf, surf1);
        surf := s1[1];
        
        s2 := DisjointUnion(surf, surf2);
        surf := s2[1];
        
        J2 := ShallowCopy(Faces(surf));
        
        SubtractSet(J2,J1);
        
        surf := RemoveFaces(surf,[j]);
        
        params[1][j] := [];
        params[1][J2[1]] := [v1, res[1], res[2], Crossproduct(res[1]-v1,res[2]-v1), VerticesOfFace(surf,J2[1])];
        params[1][J2[2]] := [v2, res[1], res[2], Crossproduct(res[1]-v2,res[2]-v2), VerticesOfFace(surf,J2[2])];
        
         # remove faces that have been replaced
        Remove(params[2],Position(params[2],j));
        Add(params[2], J2[1],J2[1]);
        Add(params[2], J2[2], J2[2]);
        
        return [surf,params];
        
    end
);

InstallMethod( FixSelfIntersections,
    [IsTriangularComplex and IsNotEdgeRamified, IsRecord, IsList, IsList, IsList, IsFloat],
    function(surface, printRecord, params, inters, numb_triangles, epsilon)
        local current, target, c_normal, t_normal, j, k, d, edge_param, res1, res2, s1, s2;
        
        
        j := numb_triangles[1];
        k := numb_triangles[2];
        current := params[1][j];
        target := params[1][k];
        
        c_normal := current[4];
        t_normal := target[4];
        
        d := current[1]*c_normal;
        
        edge_param := inters[1]-inters[2];
        res1 := PushLineToEdges(current[1], current[2], current[3], inters[1], inters[1]-inters[2], epsilon);
        #if numb_triangles = [40,58] then
            #Throw(Error);
        #fi;  
        if res1[3] then
            s1 := RetriangulateEasy(surface, printRecord, params,inters, res1, [numb_triangles[1], epsilon]);
        else
            s1 := Retriangulate(surface, printRecord, params,inters, res1, [numb_triangles[1], epsilon]);
        fi;
        params := s1[2];
        surface := ShallowCopy(s1[1]);  
        #if numb_triangles = [40,58] then
            #Throw(Error);
        #fi;    
        res2 := PushLineToEdges(target[1], target[2], target[3], inters[2], inters[2]-inters[1], epsilon);
        
        IsNotEdgeRamified(surface);
        IsSimplicialSurface(surface);
        IsTriangularComplex(surface);
        if res2[3] then
            s2 :=  RetriangulateEasy(surface, printRecord, params,inters, res2, [numb_triangles[2], epsilon]);
        else
            s2 :=  Retriangulate(surface, printRecord, params,inters, res2, [numb_triangles[2], epsilon]);
        fi;
            
        surface := s2[1]; 
        params := s2[2];
        
        
        return [surface,params];
    end
);

InstallMethod( TestTrianglesConstellation,
    [IsList,IsInt],
    function(vertices, i)
        local temp, pRecord, surf, surf2, VofEdges, EofFaces, a, numb, j;
        # good way to test errors, only working for 2 now
        numb := [2..i];
        surf := SimplicialSurfaceByVerticesInFaces( [[1,2,3]] );
        
        for j in numb do
            # need to do annoying calculations to not have the indices of the vertices shift up, TEST
            
            a := (j-1)*3;
            VofEdges := [];
            EofFaces := [];
            VofEdges[1+a] := [1+(j-1)*3,2+(j-1)*3];
            VofEdges[2+a] := [2+(j-1)*3,3+(j-1)*3];
            VofEdges[3+a] := [1+(j-1)*3,3+(j-1)*3];
            EofFaces[j] := [1+(j-1)*3,2+(j-1)*3,3+(j-1)*3];
            
            surf2 := SimplicialSurfaceByDownwardIncidence( VofEdges, EofFaces );
            temp := DisjointUnion(surf, surf2);
            surf := temp[1];
        od;

        pRecord := SetVertexCoordiantes3D(surf, vertices, rec());
        DrawSurfaceToSTLCalculate(surf,"test_constellation.txt",pRecord, false);
    end    
);

InstallMethod( EdgeNotPerpendicularToFace,
    [IsList, IsList, IsFloat, IsList,  IsFloat],
    function(c_coords, c_normal, d1, t_coords, eps)
        local edge_param, min, l;
        
        # if an edge is perpendicuclar to the face, we do not count it as an intersection
        for l in [1..3] do
            edge_param := t_coords[l mod 3 + 1]-t_coords[l];
            # test if an edges is perpendicuclar to the face
            if FlEq(AbsoluteValue(edge_param * c_normal),0., eps) then
                # test if a vertex of this edge is on the plane
                min := Minimum(AbsoluteValue(t_coords[l mod 3 + 1]*c_normal-d1),AbsoluteValue(t_coords[l]*c_normal-d1));
                # if one is, the edge is one the plane as well
                if FlEq(min,0.,eps) then
                    return false;
                fi;
            fi;
        od;
        return true;
    end    
);

InstallMethod( CheckSelfIntersections,
    "for a triagular complex without edge ramifications, a filename and a record",
    [IsTriangularComplex and IsNotEdgeRamified, IsRecord, IsList, IsString,],
    function(surface, printRecord, Coords, fileName)
        local ints, FaceIntersections, ccoords, orthog, orthog2, normal, c_normal, c_edges, t_normal, vertsOfFace, edge, edge_param, edge_param2, possb_intersec, failed, ints_points, step, steps, name, numb_int, one, c_coords, t_coords, vOedge, vOedge2, dist, res, res2, lambda, lambda2, val_1, val_2, A, p, p2, x, y, v, d1, d2, min, I, o, o1, o2, i, j, k, l, eps;
            ints := false;
            
            ints_points := [];
            eps := 1.0/(10^(6));
            steps := 0;
            
            possb_intersec := 0;
            
            I := ShallowCopy(Faces(surface));
            for j in I do      
                for k in I do
                    # set coords again since the coordinate matrix may change every iteration
                    
                    if Coords[j] <> [] then
                        # if Coords[j] = [] then j is not in I anymore
                        c_normal := Coords[j][4];
                        d1 := Coords[j][1]*c_normal;
                    
                        c_coords := [Coords[j][1],Coords[j][2],Coords[j][3]];
                    fi;
                    
                    step := [];
                    t_coords := [Coords[k][1],Coords[k][2],Coords[k][3]];
                    # cant intersect if incident
                    if k <> j and j in I and EdgeNotPerpendicularToFace(c_coords, c_normal, d1, t_coords, eps) then
                        # need to rewrite to work with coords only and half-covered-edges
                        t_normal := Coords[k][4];
                        d2 := Coords[k][1]*t_normal;
                    
                        orthog := [Crossproduct(c_coords[2]-c_coords[1],c_normal),Crossproduct(c_coords[3]-c_coords[2],c_normal),Crossproduct(c_coords[1]-c_coords[3],c_normal)];
                        
                        orthog[1] := orthog[1] / Sqrt(orthog[1]*orthog[1]);
                        orthog[2] := orthog[2] / Sqrt(orthog[2]*orthog[2]);
                        orthog[3] := orthog[3] / Sqrt(orthog[3]*orthog[3]);
                        
                        # must be right of planes, need to have the orthogonal vectors point to the inside of the triangle
                        orthog[1] := orthog[1] * ((orthog[1]*c_coords[3]-orthog[1]*c_coords[1]) / AbsoluteValue(orthog[1]*c_coords[3]-orthog[1]*c_coords[1]));
                        orthog[2] := orthog[2] * ((orthog[2]*c_coords[1]-orthog[2]*c_coords[2]) / AbsoluteValue(orthog[2]*c_coords[1]-orthog[2]*c_coords[2]));
                        orthog[3] := orthog[3] * ((orthog[3]*c_coords[2]-orthog[3]*c_coords[3]) / AbsoluteValue(orthog[3]*c_coords[2]-orthog[3]*c_coords[3]));
                         
                        # check if triangle k intersects with j
                        numb_int := 0;
                        ints_points := [];
                        # use this to compute if both of the intersection points lie on an edge
                        not_on_edge := [true, true, true];
                        for l in [1..3] do

                               
                                vOedge := [Coords[k][l],Coords[k][l mod 3 + 1]];
                                
                                # if faces are connected by a vertex, and current edge is incident to that vertex,
                                # that edge can't be used to detect intersections
                                
                                if ForAll(vOedge,x-> not x in c_coords) then
                                    edge_param := Coords[k][l mod 3 + 1]-Coords[k][l];
                                    
                                    
                                    
                                    # find point that intersects with plane
                                    res := ProjectLineOnTriangle(Coords[k][l],edge_param, d1, c_normal);
                                    p := res[1];
                                    lambda := res[2];
                                        
                                    
                                    
                                    o := [orthog[1]*p-orthog[1]*c_coords[1], orthog[2]*p-orthog[2]*c_coords[2], orthog[3]*p-orthog[3]*c_coords[3]];
                                    
                                    # check if point inside triangle
                                     
                                    if FlGeq(Minimum(o[1],o[2],o[3]),0.,eps) and FlGeq(lambda,0.,eps) and FlLeq(lambda,1.,eps) then
                                    
                                        not_on_edge[1] := not_on_edge[1] and FlEq(0.,o[1],eps); 
                                        not_on_edge[2] := not_on_edge[2] and FlEq(0.,o[2],eps); 
                                        not_on_edge[3] := not_on_edge[3] and FlEq(0.,o[3],eps); 
                                        
                                        ints := true;
                                        possb_intersec := possb_intersec + 1;
                                        numb_int := numb_int + 1;
                                        ints_points[numb_int] := p;
                                    fi;
                                        
                                    
                                fi;
                             
                        od;
                        
                        not_on_edges := not( not_on_edge[1] or not_on_edge[2] or not_on_edge[3]);
                        
                        if numb_int = 2 then
                            
                            ints_points := NumericalUniqueListOfLists(ints_points,eps);
                            
                            if Length(ints_points) = 2 and not_on_edges then
                                # if there is not second point, there is no intersection
                                IsNotEdgeRamified(surface);
                                IsSimplicialSurface(surface);
                                IsTriangularComplex(surface);
                                
                                step := FixSelfIntersections(surface, printRecord, [Coords,I], ints_points, [j,k] , eps);
                                surface := step[1];
                                Coords := step[2][1];
                                I := step[2][2];
                                steps := steps + 1;
                                
                                #if steps > 11 then
                                    #Throw(Error);
                                #fi;
                                
                                IsNotEdgeRamified(surface);
                                IsSimplicialSurface(surface);
                                IsTriangularComplex(surface);
                                
                                Print(Concatenation("\n",String(steps)));
                                
                                if steps mod 50 = 0 then
                                    name := Concatenation(fileName, "_step_", String(steps));
                                    Print("\t", possb_intersec," intersections found, ", Length(Faces(surface)), " faces in model\n");
                                    DrawSTL(name, Coords);
                                fi;

                                if steps > 1400 then
                                    Print("\n", possb_intersec," intersections found in ", String(steps), " iterations (stopped early)");
            
                                    return [ints,surface, Coords];
                                fi;
                            else 
                                # wrongly counted
                                possb_intersec := possb_intersec - numb_int;
                            fi;
                            
                        fi;
                        
                        if numb_int = 1 then
                            # need to find second point that parameterises the intersection
                            
                            d2 := Coords[k][1]*t_normal;
                            
                            orthog2 := [Crossproduct(t_coords[2]-t_coords[1],t_normal),Crossproduct(t_coords[3]-t_coords[2],t_normal),Crossproduct(t_coords[1]-t_coords[3],t_normal)];
                            
                            orthog2[1] := orthog2[1] / Sqrt(orthog2[1]*orthog2[1]);
                            orthog2[2] := orthog2[2] / Sqrt(orthog2[2]*orthog2[2]);
                            orthog2[3] := orthog2[3] / Sqrt(orthog2[3]*orthog2[3]);
                            
                            # correct orientation of normals
                            orthog2[1] := orthog2[1] * ((orthog2[1]*t_coords[3]-orthog2[1]*t_coords[1]) / AbsoluteValue(orthog2[1]*t_coords[3]-orthog2[1]*t_coords[1]));
                            orthog2[2] := orthog2[2] * ((orthog2[2]*t_coords[1]-orthog2[2]*t_coords[2]) / AbsoluteValue(orthog2[2]*t_coords[1]-orthog2[2]*t_coords[2]));
                            orthog2[3] := orthog2[3] * ((orthog2[3]*t_coords[2]-orthog2[3]*t_coords[3]) / AbsoluteValue(orthog2[3]*t_coords[2]-orthog2[3]*t_coords[3]));
                            
                            
                            for l in [1..3] do
                                vOedge2 := [Coords[j][l],Coords[j][l mod 3 + 1]];
                                
                                edge_param2 := Coords[j][l mod 3 + 1]-Coords[j][l];
                                
                                res2 := ProjectLineOnTriangle(Coords[j][l],edge_param2, d2, t_normal);
                                p2 := res2[1];
                                lambda2 := res2[2];
                                        
                                o2 := [orthog2[1]*p2-orthog2[1]*t_coords[1], orthog2[2]*p2-orthog2[2]*t_coords[2], orthog2[3]*p2-orthog2[3]*t_coords[3]];
                                        
                                # check if point inside triangle
                                                                                
                                if FlGeq(Minimum(o2[1],o2[2],o2[3]),0.,eps) and FlGeq(lambda2,0.,eps) and FlLeq(lambda2,1.,eps) then
                                
                                    not_on_edge[1] := not_on_edge[1] and FlEq(0.,o2[1],eps); 
                                    not_on_edge[2] := not_on_edge[2] and FlEq(0.,o2[2],eps); 
                                    not_on_edge[3] := not_on_edge[3] and FlEq(0.,o2[3],eps); 
                                    
                                    possb_intersec := possb_intersec + 1;
                                    numb_int := numb_int + 1;
                                    ints_points[numb_int] := p2;
                                fi;
                            od;
                            not_on_edges := not( not_on_edge[1] or not_on_edge[2] or not_on_edge[3]);
                            
                            # need to to remove (numerical) duplicate entries that are possible in certain geometrical constellations
                            ints_points := NumericalUniqueListOfLists(ints_points,eps);
                            
                            if Length(ints_points) = 2 and not_on_edges then
                                # if there is not second point, there is no intersection
                                IsNotEdgeRamified(surface);
                                IsSimplicialSurface(surface);
                                IsTriangularComplex(surface);
                                
                                
                                step := FixSelfIntersections(surface, printRecord, [Coords,I], ints_points, [j,k] , eps);
                                surface := step[1];
                                Coords := step[2][1];
                                I := step[2][2];
                                steps := steps + 1;
                                
                                #if steps > 11 then
                                    #Throw(Error);
                                #fi;
                            
                                IsNotEdgeRamified(surface);
                                IsSimplicialSurface(surface);
                                IsTriangularComplex(surface);
                                Print(Concatenation("\n",String(steps)));
                                
                                if steps mod 50 = 0 then
                                    name := Concatenation(fileName, "_step_", String(steps));
                                    Print("\t", possb_intersec," intersections found, ", Length(Faces(surface)), " faces in model\n");
                                    DrawSTL(name, Coords);
                                fi;
                                
                                
                                if steps > 1400 then
                                    Print("\n", possb_intersec," intersections found in ", String(steps), " iterations (stopped early)");
            
                                    return [ints,surface, Coords];
                                fi;
                            else 
                                # wrongly counted
                                possb_intersec := possb_intersec - numb_int;
                            fi;
                        fi;
                        
                        #only check one triangle since the loop goes through all of them
                        
                    fi;
                od;
            od;
            
            Print("\n", possb_intersec," intersections found in ", String(steps), " iterations");
            
            return [ints,surface, Coords];
    end
);

InstallMethod(DrawSTL,
    [IsString, IsList],
    function(fileName, Coords)
        local file, filesepr, name, output, i, j, k, coords, normal;
    
        filesepr := SplitString(fileName, ".");
        name := filesepr[1];
        # test file name
        file := Filename( DirectoryCurrent(), Concatenation(name,".stl") );
        output := OutputTextFile( file, false ); # override other files
            
        if output = fail then
            Error(Concatenation("File ", String(file), " can't be opened.") );
        fi;
        
        AppendTo(output, Concatenation("solid ", name, "\n"));
        
        for i in [1..Length(Coords)] do
            if IsBound(Coords[i]) and Coords[i] <> [] then
            
                # get coords of vertices
                coords := [Coords[i][1],Coords[i][2],Coords[i][3]];
                normal := Coords[i][4];
                
                # write normal
                AppendTo(output, "\tfacet normal ");
                for j in [1..3] do
                    AppendTo(output, Concatenation(String(normal[j])," "));
                od;
                AppendTo(output, "\n");
                    
                # write vertex coords
                AppendTo(output, "\t\touter loop\n");
                for j in [1..3] do
                    AppendTo(output,"\t\t\tvertex ");
                       
                    for k in [1..3] do
                        AppendTo(output, Concatenation(String(coords[j][k])," "));
                    od;
                    AppendTo(output,"\n");
                od;
                AppendTo(output, "\t\tendloop\n");
            AppendTo(output,"\tendfacet\n");
            fi;
        od;
        AppendTo(output, Concatenation("endsolid ", name));
        CloseStream(output);
        return;
    end
);

InstallMethod( DrawSurfaceToSTLCalculate,
    "for a polygonal complex without edge ramifications, a filename and a record",
    [IsTriangularComplex and IsNotEdgeRamified, IsString, IsRecord, IsBool],
    function(surface, fileName, printRecord, test)
        local file, filesepr, name, output, Coords, intersections, ccoords, coords, i, j, k, vertsOfFace, coord1, coord2, coord3, x, y, normal, intersects;
            
            Coords := [];
            
            # get data
            for i in [1..(NumberOfFaces(surface))] do
                if IsFaceActive(surface, i, printRecord) then
                    vertsOfFace := VerticesOfFaces(surface)[i];
                    # get coords of current vertices
                    ccoords := [GetVertexCoordiantes3DNC(vertsOfFace[1], printRecord), GetVertexCoordiantes3DNC(vertsOfFace[2], printRecord),GetVertexCoordiantes3DNC(vertsOfFace[3], printRecord)];
                    # get normal 
                    x := ccoords[2]-ccoords[1];
                    y := ccoords[3]-ccoords[1];
                    normal := Crossproduct(x,y);
                    normal := normal / Sqrt(normal*normal);
                    ccoords[4] := normal;
                    ccoords[5] := vertsOfFace;
                    
                    Coords[i] := ccoords;
                fi;
            od;
            Print(Coords,"\nOver");
            DrawSTL(Concatenation(fileName,"_unchanged"), Coords);
            # check for self intersections
            if test then
                intersections := CheckSelfIntersections(surface, printRecord, Coords, fileName);
                intersects := intersections[1];
                # CheckSelfIntersections(intersections[2], printRecord, intersections[3], fileName);
                surface := intersections[2];
                Coords := intersections[3];
                if intersects then
                    Print("\nSelf-Intersections present, produced ", Length(Faces(surface)), " faces");
                fi;
            fi;
           
            DrawSTL(fileName, Coords);
            return printRecord;
    end
);

InstallMethod( DrawSurfaceToSTL,
    "for a polygonal complex without edge ramifications, a filename and a record",
    [IsTriangularComplex and IsNotEdgeRamified, IsString, IsRecord, IsBool],
    function(surface, fileName, printRecord, test)
        return DrawSurfaceToSTLCalculate(surface,fileName,printRecord, test);
    end
);

InstallMethod( TestEasy,
    [],
    function()
        local oct, tet, verticesPositions, pRecord, fileName;
        oct := Octahedron();
        verticesPositions :=[
                            [ 0, 0, Sqrt(2.) ],
                            [ 1, 1, 0 ],
                            [ 1, -1, 0 ],
                            [ -1, -1, 0 ],
                            [ -1, 1, 0 ],
                            [ 0, 0, -Sqrt(2.) ] ];
        pRecord := SetVertexCoordiantes3D(oct, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(oct,"octahedron",pRecord, true);
        tet := Tetrahedron();
        verticesPositions := [
                                [ 1, -1/Sqrt(3.), -1/Sqrt(6.) ],
                                [ -1, -1/Sqrt(3.), -1/Sqrt(6.) ],
                                [ 0, 2/Sqrt(3.) , -1/Sqrt(6.) ],
                                [ 0, 0, 3/Sqrt(6.) ] ];
        pRecord := SetVertexCoordiantes3D(tet, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(tet, "tetrahedron.txt", pRecord, true);
    end
);

InstallMethod( TestIco_21_32,
    [],
    function()
        local ico, Coord2_1, Coord3_2, pRecord1, pRecord2, fileName;
        
        ico := Icosahedron();
        
        Coord3_2:= [
                    [  0.9510565160,  0.0000000000,  0.0000000000 ],
                    [  0.4253254040,  0.8506508085,  0.0000000000 ],
                    [  0.4253254040,  0.2628655560,  0.8090169940 ],
                    [ -0.0449027976, -0.0277514551,  0.0854101965 ],
                    [  0.4253254040, -0.6881909604, -0.4999999998 ],
                    [  0.4253254040, -0.6881909604,  0.4999999998 ],
                    [ -0.4253254040,  0.6881909604,  0.4999999998 ],
                    [ -0.4253254040,  0.6881909604, -0.4999999998 ],
                    [ -0.4253254040, -0.2628655560, -0.8090169940 ],
                    [ -0.4253254040, -0.8506508085,  0.0000000000 ],
                    [  0.0449027976,  0.0277514551, -0.0854101965 ],
                    [ -0.9510565160,  0.0000000000,  0.0000000000 ],
                    ];
        pRecord1 := SetVertexCoordiantes3D(ico, Coord3_2, rec());
        DrawSurfaceToSTLCalculate(ico,"icosahedron_3_2",pRecord1, true);
        
        Coord2_1 :=[
                    [  0.5877852523,  0.0000000000,  0.0000000000 ],
                    [ -0.2628655561,  0.5257311121,  0.0000000000 ],
                    [ -0.2628655561, -0.4253254041,  0.3090169943 ],
                    [ -0.2628655561, -0.4253254041, -0.3090169943 ],
                    [ -0.2628655561,  0.1624598481,  0.4999999999 ],
                    [ -0.2628655561,  0.1624598481, -0.4999999999 ],
                    [  0.2628655561, -0.1624598481, -0.4999999999 ],
                    [  0.2628655561, -0.1624598481,  0.4999999999 ],
                    [  0.2628655561,  0.4253254041, -0.3090169943 ],
                    [  0.2628655561, -0.5257311121,  0.0000000000 ],
                    [  0.2628655561,  0.4253254041,  0.3090169943 ],
                    [ -0.5877852523,  0.0000000000,  0.0000000000 ],
                    ];
        pRecord2 := SetVertexCoordiantes3D(ico, Coord2_1, rec());
        DrawSurfaceToSTLCalculate(ico,"icosahedron_2_1",pRecord2, true);
        
    end
);


InstallMethod( TestIco_11_31,
    [],
    function()
        local ico, Coord1_1, Coord3_1, pRecord1, pRecord2, fileName, Coord3_2;
        
        ico := Icosahedron();
        
        Coord1_1:= [
                    [  0.0000000000, -0.0564956995,  0.4967979831 ],
                    [  0.0000000000,  0.0564956995, -0.4967979831 ],
                    [ -0.8660254035,  0.0000000000,  0.0000000000 ],
                    [  0.8660254035,  0.0000000000,  0.0000000000 ],
                    [  0.2379137858,  0.7781232521,  0.0000000000 ],
                    [ -0.5739569990,  0.7577886993,  0.5834829341 ],
                    [ -0.2379137858, -0.7781232521,  0.0000000000 ],
                    [  0.5739569990, -0.7577886993, -0.5834829341 ],
                    [  0.2886751345,  0.1765267750, -0.7971856525 ],
                    [ -0.4688236925,  0.0868487119, -0.1505378568 ],
                    [ -0.2886751345, -0.1765267750,  0.7971856525 ],
                    [  0.4688236925, -0.0868487119,  0.1505378568 ],
                    ];
        pRecord1 := SetVertexCoordiantes3D(ico, Coord1_1, rec());
        DrawSurfaceToSTLCalculate(ico,"icosahedron_1_1",pRecord1, true);
        
        
        
        Coord3_1:= [
                    [  0.5877852523,  0.0000000000,  0.0000000000 ],
                    [ -0.2628655561,  0.5257311121,  0.0000000000 ],
                    [ -0.2628655561, -0.4253254041,  0.3090169943 ],
                    [  0.4979796570,  0.8057480107,  0.5854101964 ],
                    [ -0.2628655561,  0.1624598481,  0.4999999999 ],
                    [ -0.2628655561,  0.1624598481, -0.4999999999 ],
                    [  0.2628655561, -0.1624598481, -0.4999999999 ],
                    [  0.2628655561, -0.1624598481,  0.4999999999 ],
                    [  0.2628655561,  0.4253254041, -0.3090169943 ],
                    [  0.2628655561, -0.5257311121,  0.0000000000 ],
                    [ -0.4979796570, -0.8057480107, -0.5854101964 ],
                    [ -0.5877852523,  0.0000000000,  0.0000000000 ],
                    ];
        pRecord2 := SetVertexCoordiantes3D(ico, Coord3_1, rec());
        DrawSurfaceToSTLCalculate(ico,"icosahedron_3_1",pRecord2, true);
        
    end
);


InstallMethod( TestIntersections,
    [],
    function()
        local surf, verticesPositions, pRecord;
        surf := SimplicialSurfaceByVerticesInFaces( [[1,2,3],[4,5,6]] );
        verticesPositions :=[[ -1.0000000000,  0.0000000000,  0.0000000000 ],
                            [ 0.0000000000, 1.0000000000 ,  0.0000000000 ],
                            [ 1.0000000000, 0.0000000000,  0.0000000000 ],
                            [ 0.0000000000, -0.2000000000, -1.000000000 ],
                            [ 0.0000000000, 1.0000000000 ,  0.0000000000 ],
                            [ 0.0000000000, -0.2000000000, 1.0000000000 ],
                            ];
        
        pRecord := SetVertexCoordiantes3D(surf, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(surf,"intersections1",pRecord, true);
        
        verticesPositions :=[ [  0.5877852523,  0.0000000000,  0.0000000000 ],
                            [ -0.2628655561, -0.4253254041, -0.3090169943 ],
                            [ -0.2628655561,  0.1624598481,  0.4999999999 ],
                            [  0.5877852523,  0.0000000000,  0.0000000000 ],
                            [ -0.2628655561,  0.5257311121,  0.0000000000 ],
                            [ -0.2628655561, -0.4253254041,  0.3090169943 ],
                            ];
        pRecord := SetVertexCoordiantes3D(surf, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(surf,"brain_fail",pRecord, true);
    end
);

InstallMethod( TestTriang2,
    [],
    function()
        local surf, verticesPositions, pRecord;
        surf := SimplicialSurfaceByVerticesInFaces( [[1,2,3],[4,5,6]] );
        verticesPositions :=[[ -1.0000000000,  -0.2000000000,  0.0000000000 ],
                            [ 0.0000000000, 1.0000000000 ,  0.0000000000 ],
                            [ 1.0000000000,  -0.2000000000,  0.0000000000 ],
                            [ 0.0000000000, -1.00000000, 0.500000000 ],
                            [ 0.3000000000, 0.4000000000 ,  0.7000000000 ],
                            [ 0.0000000000, 0.0000000000, -1.0000000000 ],
                            ];

        pRecord := SetVertexCoordiantes3D(surf, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(surf,"retriangulation2",pRecord, true);
    end
);

InstallMethod( TestTriang4,
    [],
    function()
        local surf, verticesPositions, pRecord;
        surf := SimplicialSurfaceByVerticesInFaces( [[1,2,3],[4,5,6]] );
        verticesPositions :=[[ -1.0000000000,  -0.2000000000,  0.0000000000 ],
                            [ 0.0000000000, 1.0000000000 ,  0.0000000000 ],
                            [ 1.0000000000,  -0.2000000000,  0.0000000000 ],
                            [ -0.2000000000, -0.100000000, 0.500000000 ],
                            [ 0.2000000000, -0.150000000 ,  0.3000000000 ],
                            [ 0.0000000000, 0.0000000000, -0.5000000000 ],
                            ];

        pRecord := SetVertexCoordiantes3D(surf, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(surf,"retriangulation4",pRecord, true);
    end
);

InstallMethod( TestOcta1,
    [],
    function()
        local surf, verticesPositions, pRecord;
        surf := SimplicialSurfaceByVerticesInFaces( [[1,2,3],[4,5,6],[7,8,9],[10,11,12],[13,14,15],[16,17,18],[19,20,21],[22,23,24],[25,26,27],[28,29,30]] );
        verticesPositions :=[[  0.95105651599999996, 0., 0.   ],
                             [  0.42532540400000002, 0.85065080849999997, 0.  ],
                             [  -0.044902797600000002, -0.027751455099999999, 0.085410196499999994    ],
                             [  0.95105651599999996, 0., 0.  ],
                             [  -0.044902797600000002, -0.027751455099999999, 0.085410196499999994  ],
                             [  0.42532540400000002, -0.68819096040000005, -0.49999999979999998    ],
                             [0.42532540400000002, 0.85065080849999997, 0.],
                             [-0.044902797600000002, -0.027751455099999999, 0.085410196499999994],
                             [-0.42532540400000002, 0.68819096040000005, -0.49999999979999998],
                             [-0.044902797600000002, -0.027751455099999999, 0.085410196499999994],
                             [0.42532540400000002, -0.68819096040000005, -0.49999999979999998],
                             [-0.42532540400000002, -0.262865556, -0.80901699400000004],
                             [-0.044902797600000002, -0.027751455099999999, 0.085410196499999994],
                             [-0.42532540400000002, 0.68819096040000005, -0.49999999979999998],
                             [-0.42532540400000002, -0.262865556, -0.80901699400000004],
                             [0.42532540400000002, 0.262865556, 0.80901699400000004 ],
                             [0.42532540400000002, -0.68819096040000005, 0.49999999979999998],
                             [0.044902797600000002, 0.027751455099999999, -0.085410196499999994],
                             [0.42532540400000002, -0.68819096040000005, 0.49999999979999998],
                             [-0.42532540400000002, -0.85065080849999997, 0.],
                             [0.044902797600000002, 0.027751455099999999, -0.085410196499999994],
                             [0.42532540400000002, 0.262865556, 0.80901699400000004],
                             [-0.42532540400000002, 0.68819096040000005, 0.49999999979999998],
                             [0.044902797600000002, 0.027751455099999999, -0.085410196499999994],
                             [-0.42532540400000002, -0.85065080849999997, 0.],
                             [0.044902797600000002, 0.027751455099999999, -0.085410196499999994],
                             [-0.95105651599999996, 0., 0.],
                             [-0.42532540400000002, 0.68819096040000005, 0.49999999979999998],
                             [0.044902797600000002, 0.027751455099999999, -0.085410196499999994],
                             [-0.95105651599999996, 0., 0.]
                            ];

        pRecord := SetVertexCoordiantes3D(surf, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(surf,"testOcta",pRecord, true);
    end
);

InstallMethod( TestOcta2,
    [],
    function()
        local surf, verticesPositions, pRecord;
        
        surf := SimplicialSurfaceByVerticesInFaces( [[1,2,3],[4,5,6]] );
        verticesPositions :=[[-0.044902797600000002, -0.027751455099999999, 0.085410196499999994   ],
                            [ -0.42532540400000002, 0.68819096040000005, -0.49999999979999998 ],
                            [ -0.42532540400000002, -0.262865556, -0.80901699400000004 ],
                            [ -0.42532540400000002, -0.85065080849999997, 0. ],
                            [ 0.044902797600000002, 0.027751455099999999, -0.085410196499999994  ],
                            [ -0.95105651599999996, 0., 0. ],
                            ];

        pRecord := SetVertexCoordiantes3D(surf, verticesPositions, rec());
        DrawSurfaceToSTLCalculate(surf,"testOcta2",pRecord, true);
    end
);
